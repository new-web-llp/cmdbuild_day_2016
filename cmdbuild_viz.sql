select '
digraph G {
  graph [rankdir=BT, size="12.8,5.2", page="12.8,5.2", margin="0.00,0.00", ratio="fill", orientation=portrait ]
';

select '  subgraph cluster_data_model {
    graph [style=dotted];
    node [shape=record,fillcolor="#b7c9e3",style=filled];
    edge [arrowhead=onormal];
';


select '      ' || cnm || ' [label="{' || cnm || '|{'  || regexp_replace(regexp_replace(lbl::text,'([^{},])+','<\&> +\&','g')::text,',','|','g') || '}}" ]'
from (
  select a.classname as cnm, array_agg(a.attributename) as lbl
  from system_attributecatalog as a, (
    SELECT  domainclass1 as reffrom , domainclass2 as refto, domainname as refname FROM system_domaincatalog WHERE domaincardinality in ( 'N:1', 'N:N' )
    UNION
    SELECT  domainclass2 as reffrom , domainclass1 as refto, domainname as refname FROM system_domaincatalog WHERE domaincardinality = '1:N'
  ) as b
  where (a.classname not like '\_%' and a.classname not in ('Metadata','Patch','Menu','Email')) and a.classname=b.reffrom and b.refto=a.attributereference and a.attributereferencedomain = b.refname group by 1
) as lists;


select '      '|| child|| ' -> '|| parent || ';' from system_treecatalog where child not like '\_%' and parent not like '\_%' and child not in ('Metadata','Patch','Menu','Email') and parent not in ('Metadata','Patch','Menu','Email') order by 1 asc;

select '      edge [dir=both, arrowhead=none, arrowtail=crow,color="#' || lpad(to_hex(floor(16760832*random())::integer+255)::text,6,'0') ||'"] '|| a.classname ||':'|| a.attributename ||' -> ' || b.refto||';'
  from system_attributecatalog as a, (
    SELECT  domainclass1 as reffrom , domainclass2 as refto, domainname as refname FROM system_domaincatalog WHERE domaincardinality in ( 'N:1', 'N:N' )
    UNION
    SELECT  domainclass2 as reffrom , domainclass1 as refto, domainname as refname FROM system_domaincatalog WHERE domaincardinality = '1:N'
  ) as b
  where (a.classname not like '\_%' and a.classname not in ('Metadata','Patch','Menu','Email')) and a.classname=b.reffrom and b.refto=a.attributereference and a.attributereferencedomain = b.refname order by 1;


select '
      label="Data Model";
    }
';

select '
}';
