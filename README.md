A SQL script which produces the GraphViz dot-file. To visualize CMDBuild data model.

Usage:

psql -U postgres -d cmdbuild -h hostname  -qAt < cmdbuild_viz.sql | dot -Tsvg > data_model.svg
